<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MonProfilController extends AbstractController
{
    /**
     * @Route("/monprofil", name="mon_profil")
     */
    public function index(): Response
    {
        $user=new User();
             
        $sujets=$user->getSujets();
        dump($sujets);
        return $this->render('mon_profil/index.html.twig', [
            'sujets' =>$sujets ,
        ]);
    }
}
