<?php

namespace App\Controller;

use App\Entity\Sujets;
use App\Form\SujetsType;
use App\Repository\SujetsRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sujets")
 */
class SujetsController extends AbstractController
{
    /**
     * @Route("/", name="sujets_index", methods={"GET"})
     */
    public function index(SujetsRepository $sujetsRepository): Response
    {
        return $this->render('sujets/index.html.twig', [
            'sujets' => $sujetsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sujets_new", methods={"GET","POST"})
     */
    public function new(Request $request,UserRepository $userRepository): Response
    {
        $sujet = new Sujets();
        $form = $this->createForm(SujetsType::class, $sujet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userRepository=$this->getUser();
            $entityManager = $this->getDoctrine()->getManager();
            $sujet->setCreatedBy($userRepository);
            $entityManager->persist($sujet);
            $entityManager->flush();

            return $this->redirectToRoute('sujets_index');
        }

        return $this->render('sujets/new.html.twig', [
            'sujet' => $sujet,
            'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/{id}", name="sujets_show", methods={"GET"})
    //  */
    // public function show(Sujets $sujet): Response
    // {
    //     return $this->render('sujets/show.html.twig', [
    //         'sujet' => $sujet,
    //     ]);
    // }

        /**
     * @Route("/{id}", name="sujets_show", methods={"GET"})
     */
    public function show(Sujets $sujet): Response
    {
        return $this->render('sujets/show.html.twig',
        [
            'sujet'=>$sujet,
        ]);
    }


    /**
     * @Route("/{id}/edit", name="sujets_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sujets $sujet): Response
    {
        $form = $this->createForm(SujetsType::class, $sujet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sujets_index');
        }

        return $this->render('sujets/edit.html.twig', [
            'sujet' => $sujet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sujets_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sujets $sujet): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sujet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sujet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sujets_index');
    }
}
