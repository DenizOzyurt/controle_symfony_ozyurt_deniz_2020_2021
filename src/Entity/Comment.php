<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $commontaire;

    /**
     * @ORM\ManyToOne(targetEntity=Sujets::class, inversedBy="quiFait")
     */
    private $quelleSujet;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     */
    private $relation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommontaire(): ?string
    {
        return $this->commontaire;
    }

    public function setCommontaire(string $commontaire): self
    {
        $this->commontaire = $commontaire;

        return $this;
    }

    public function getQuelleSujet(): ?Sujets
    {
        return $this->quelleSujet;
    }

    public function setQuelleSujet(?Sujets $quelleSujet): self
    {
        $this->quelleSujet = $quelleSujet;

        return $this;
    }

    public function getRelation(): ?User
    {
        return $this->relation;
    }

    public function setRelation(?User $relation): self
    {
        $this->relation = $relation;

        return $this;
    }
}
