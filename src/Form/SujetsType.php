<?php

namespace App\Form;

use App\Entity\Sujets;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SujetsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topic')
            ->add('content')
            ->add('createdAt',DateType::class,[
                'widget'=>'single_text'
            ])
            // ->add('updatedAt', DateType::class, [
            //     'widget'=>'single_text'
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sujets::class,
        ]);
    }
}
